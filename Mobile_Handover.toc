\contentsline {chapter}{Acknowledgments}{iii}{section*.1}
\contentsline {chapter}{Abstract}{vii}{section*.2}
\contentsline {chapter}{Table of Contents}{ix}{section*.3}
\contentsline {chapter}{List of Figures}{xiii}{section*.5}
\contentsline {chapter}{List of Tables}{xv}{section*.7}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Project Goal}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Definitions and Abbreviations}{3}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Abbreviations}{3}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Definitions}{4}{subsection.1.2.2}
\contentsline {chapter}{\numberline {2}Background and Related Work}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}Mobile Networks and Wireless Communication}{7}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Local Area Networks}{7}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Wide Area Networks}{8}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Access Points}{9}{subsection.2.1.3}
\contentsline {subsubsection}{Wi-Fi Wireless Router}{9}{section*.10}
\contentsline {subsubsection}{Mobile Cell Tower}{9}{section*.12}
\contentsline {section}{\numberline {2.2}Types of Network Handover}{11}{section.2.2}
\contentsline {subsubsection}{Hard and Soft Handover}{11}{section*.15}
\contentsline {subsubsection}{Horizontal and Vertical Handover}{12}{section*.16}
\contentsline {subsubsection}{Cost of Handover}{12}{section*.18}
\contentsline {section}{\numberline {2.3}Algorithms Governing Handover}{13}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Greedy Handover Algorithm}{13}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Predictive Handover Algorithm}{14}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Location Based Handover Algorithm}{14}{subsection.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}Proposal of an Improved Location Based Technique}{14}{subsection.2.3.4}
\contentsline {chapter}{\numberline {3}Development of the Network Mapping System}{17}{chapter.3}
\contentsline {section}{\numberline {3.1}Development Process}{17}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Constraints}{17}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Requirements}{18}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Android Application Specification}{18}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Usage and Use Cases}{18}{subsection.3.2.1}
\contentsline {subsubsection}{Manual Mapping}{19}{section*.24}
\contentsline {subsubsection}{Automated Mapping}{20}{section*.25}
\contentsline {subsubsection}{Data Caching and Upload}{20}{section*.26}
\contentsline {subsection}{\numberline {3.2.2}User Interface}{21}{subsection.3.2.2}
\contentsline {subsubsection}{Network Polling Setup Screen}{21}{section*.28}
\contentsline {subsubsection}{Data Collection Screen}{22}{section*.29}
\contentsline {subsubsection}{Cache Screen}{22}{section*.30}
\contentsline {subsubsection}{Settings Screen}{22}{section*.31}
\contentsline {subsubsection}{About Screen}{23}{section*.32}
\contentsline {subsection}{\numberline {3.2.3}Components}{23}{subsection.3.2.3}
\contentsline {subsubsection}{Location Manager}{23}{section*.33}
\contentsline {subsubsection}{Network Manager}{23}{section*.34}
\contentsline {subsubsection}{Web Adapter}{24}{section*.35}
\contentsline {subsubsection}{Cache}{24}{section*.36}
\contentsline {subsubsection}{Data Point}{24}{section*.37}
\contentsline {section}{\numberline {3.3}Web Application Specification}{25}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Usage and Use Cases}{25}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}User Interface}{26}{subsection.3.3.2}
\contentsline {subsubsection}{Home Page}{27}{section*.41}
\contentsline {subsubsection}{Search Page}{27}{section*.42}
\contentsline {subsubsection}{Maps Page}{27}{section*.43}
\contentsline {subsubsection}{Script Editor Page}{28}{section*.44}
\contentsline {subsection}{\numberline {3.3.3}Components}{28}{subsection.3.3.3}
\contentsline {subsubsection}{Database Adapter}{28}{section*.45}
\contentsline {subsubsection}{REST API}{28}{section*.46}
\contentsline {subsubsection}{Data Search and Selection}{28}{section*.47}
\contentsline {subsubsection}{Dynamic Data Visualization}{29}{section*.48}
\contentsline {subsection}{\numberline {3.3.4}Data Validation}{29}{subsection.3.3.4}
\contentsline {section}{\numberline {3.4}Testing And Quality Assurance}{29}{section.3.4}
\contentsline {chapter}{\numberline {4}Data Collection}{31}{chapter.4}
\contentsline {section}{\numberline {4.1}Collected Data}{31}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Hardware and Network Provider}{33}{subsection.4.1.1}
\contentsline {section}{\numberline {4.2}Database and Formatting}{33}{section.4.2}
\contentsline {section}{\numberline {4.3}Expectations and Hypothesis}{35}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Buildings as Signal Obstructions}{35}{subsection.4.3.1}
\contentsline {section}{\numberline {4.4}Controls and Methods of Collection}{37}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Spatial Collection}{37}{subsection.4.4.1}
\contentsline {subsubsection}{Macquarie University}{37}{section*.51}
\contentsline {subsubsection}{Motorway 2}{38}{section*.52}
\contentsline {subsection}{\numberline {4.4.2}Temporal Collection}{38}{subsection.4.4.2}
\contentsline {chapter}{\numberline {5}Data Analysis}{39}{chapter.5}
\contentsline {section}{\numberline {5.1}Metadata}{39}{section.5.1}
\contentsline {section}{\numberline {5.2}Methods of Data Analysis}{39}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Google Maps API Scripts}{39}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Tableau}{40}{subsection.5.2.2}
\contentsline {section}{\numberline {5.3}Results}{40}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Variance and Quality of Data}{40}{subsection.5.3.1}
\contentsline {subsubsection}{Wi-Fi}{40}{section*.54}
\contentsline {subsubsection}{Mobile Data}{41}{section*.55}
\contentsline {subsection}{\numberline {5.3.2}Shadows}{43}{subsection.5.3.2}
\contentsline {subsubsection}{Building Shadows}{43}{section*.57}
\contentsline {subsubsection}{Topography Shadows}{44}{section*.60}
\contentsline {subsection}{\numberline {5.3.3}Cell Tower Breathing}{45}{subsection.5.3.3}
\contentsline {subsection}{\numberline {5.3.4}Handover Between 3G and 4G}{45}{subsection.5.3.4}
\contentsline {subsection}{\numberline {5.3.5}Further Observations}{46}{subsection.5.3.5}
\contentsline {subsubsection}{M2 Consistent Results}{47}{section*.63}
\contentsline {subsubsection}{Mapped Mobile Signal Strength}{47}{section*.64}
\contentsline {subsubsection}{Cell Tower Coverage}{47}{section*.65}
\contentsline {chapter}{\numberline {6}Conclusions and Future Work}{49}{chapter.6}
\contentsline {section}{\numberline {6.1}Discussion}{49}{section.6.1}
\contentsline {section}{\numberline {6.2}Conclusion}{50}{section.6.2}
\contentsline {section}{\numberline {6.3}Future Work}{51}{section.6.3}
\contentsline {chapter}{\numberline {A}Signal Mapper System Requirements}{53}{appendix.A}
\contentsline {section}{\numberline {A.1}Overview}{53}{section.A.1}
\contentsline {section}{\numberline {A.2}System Requirements}{53}{section.A.2}
\contentsline {chapter}{\numberline {B}User Interface}{57}{appendix.B}
\contentsline {chapter}{\numberline {C}Figures and Graphics}{65}{appendix.C}
\contentsline {section}{\numberline {C.1}Figures}{65}{section.C.1}
\contentsline {subsection}{\numberline {C.1.1}Wi-Fi Signal Variance}{65}{subsection.C.1.1}
\contentsline {subsection}{\numberline {C.1.2}M2 Signal Variance}{66}{subsection.C.1.2}
\contentsline {section}{\numberline {C.2}Graphics}{66}{section.C.2}
\contentsline {subsection}{\numberline {C.2.1}Signal Strength Maps}{66}{subsection.C.2.1}
\contentsline {chapter}{\numberline {D}Code Samples}{71}{appendix.D}
\contentsline {section}{\numberline {D.1}Maps API Script Example}{71}{section.D.1}
\contentsline {chapter}{Bibliography}{72}{section*.84}
